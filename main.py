import os, os,requests,json

ROOT_DIR = "public/"

TEMP = "/tmp"

TEMP_LATE = '''<a href="%s">%s</a><br>'''


def make_index_html():
    with open(os.path.join(ROOT_DIR,"index.html"),"w") as f:
        for root, dirs, files in os.walk(os.path.join(ROOT_DIR)):
            for name in files:
                file = os.path.join(root.replace(ROOT_DIR,""), name)
                if "__MACOSX" not in file:
                    f.write(TEMP_LATE % (file,file))

def download_single_arch(version,arch,binary,url):
    version_dir = os.path.join(ROOT_DIR,version)
    arch_dir = os.path.join(version_dir,arch)
    if not os.path.exists(version_dir):
        os.makedirs(version_dir)
    if not os.path.exists(arch_dir):
        os.makedirs(arch_dir)
    temp_zip_dir = os.path.join(TEMP,arch_dir)
    if not os.path.exists(temp_zip_dir):
        os.makedirs(temp_zip_dir)
    zip_name = os.path.join(temp_zip_dir,binary + ".zip")
    r = requests.get(url, stream=True)
    with open(zip_name, "wb") as _binary_file:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:
                _binary_file.write(chunk)

    os.system("unzip -o -d %s %s " % (arch_dir,zip_name))

def download_version_data(url):
    data = requests.get(url).json()
    version = data["version"]
    archs = data['bin']
    for arch in archs.keys():
        for binary in archs[arch].keys():
            url = archs[arch][binary]
            download_single_arch(version,arch,binary,url)


def get_all_versions():
    versions = requests.get("https://ffbinaries.com/api/v1").json()["versions"]
    for version in versions:
        download_version_data(versions[version])
    
    size = 0
 
    # get size

    for path, dirs, files in os.walk(ROOT_DIR):
        for f in files:
            fp = os.path.join(path, f)
            size += os.path.getsize(fp)

    print("total size = %d" % size)

if __name__ == '__main__':
    get_all_versions()
    make_index_html()
